package com.devcamp.hellodevcampworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HellodevcampworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellodevcampworldApplication.class, args);
	}

}
